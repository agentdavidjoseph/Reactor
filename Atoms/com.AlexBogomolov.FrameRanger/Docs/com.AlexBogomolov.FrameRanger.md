# Frame Ranger for Fusion

This is a script for managing ranges based on given frames offset.
Save multiple IN/OUT render ranges and switch with doubleclick.
Set amout of frame hangles to offset render range.  Set frame handles with a shortcut.



_Usage:_

* Set amount of frames handles to offset with a FrameRanger. The offset amount will be saved in comp data.
* Set/Reset frame handles with `ALT + F` shortcut or with `< >` or `> <` buttons.
* Use `> >`, `< <`,  buttons to move In/OUT range to desired direction to set amount of frames.
* Save multiple IN/OUT ranges in comp data and set them with doubleclick on a list.
* Set Global Comp range based on selected Loader with `Set from Loader` button or with `CTRL + SHIFT + F`.
* Reset global render range with `Reset Globals` button. 
* Works with Davinci Fusion too.

_Copyright:_ Alexey Bogomolov (mail@abogomolov.com)

_License:_ [MIT](https://mit-license.org/)

_Version History:_ 
v.1.0 - [2020/12/07]
v.1.5 - [2021/10/05]

_Donations:_ [PayPal.me](https://paypal.me/aabogomolov/5usd)
