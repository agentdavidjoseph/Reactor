-- Description: 
--         Saver Manager. Solo selected saver(s), deselect all or enable all. 
--         Make loader from saver (requires dedicated -- script, installed from Reactor).
--         Set Saver In and Out
-- License: MIT
-- Author: Alex Bogomolov
-- email: mail@abogomolov.com
-- Donate: paypal.me/aabogomolov
-- Version: 1.2, 2020/9/1

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width,height = 270,130
osSeparator = package.config:sub(1,1)
-- Find out the current operating system platform. The platform variable should be set to either 'Windows', 'Mac', or 'Linux'.
platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

--close UI on ESCAPE
app:AddConfig("SManager", {
    Target {
        ID = "SManager",
    },
    Hotkeys {
        Target = "SManager",
        Defaults = true,
        ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
    },
})

-- prevent multiple UIs
win = ui:FindWindow("SManager")
if win then
    win:Raise()
    win:ActivateWindow()
    return
end

win = disp:AddWindow({
    ID = 'SManager',
    TargetID = 'SManager',
    WindowTitle = 'Saver Manager',
    Geometry = {500, 400, width, height},
    Spacing = 5,
    
    ui:VGroup{
        ID = 'root',
        ui:HGroup{VMargin = 10,
          Weight = 0,
          ui:Button{ID = 'Solo', Text = 'Solo Selected',},
        },
        ui:HGroup{
          Weight = 0,
          ui.Button{ID= 'SelectAll', Text = 'Select All'},
          ui:Button{ID = 'Enable', Text = 'Enable All',},
        },
        ui:HGroup{
          Weight = 0,
          ui.Button{ID= 'CreateLoaders', Text = 'Make Loader'},
          ui.Button{ID= 'Reveal', Text = 'Reveal destination'}
        },
        ui:HGroup{
          Weight = 0,
            -- VMargin = 3,
            ui:Button{ ID = 'setIn', Text = 'saver IN', },
            ui:Button{ ID = 'setOut', Text = 'saver OUT', }
        },
     },
})

function Dirname(filename)
	return filename:match('(.*' .. tostring(osSeparator) .. ')')
end

-- Open a folder window up using your desktop file browser
function OpenDirectory(mediaDirName)
	command = nil
	dir = Dirname(mediaDirName)

	-- Open a folder view using the os native command
	if platform == 'Windows' then
		-- Running on Windows
		command = 'explorer "' .. dir .. '"'
		
		-- print("[Launch Command] ", command)
		os.execute(command)
	elseif platform == 'Mac' then
		-- Running on Mac
		command = 'open "' .. dir .. '" &'
					
		-- print("[Launch Command] ", command)
		os.execute(command)
	elseif platform == 'Linux' then
		-- Running on Linux
		command = 'nautilus "' .. dir .. '" &'
					
		-- print("[Launch Command] ", command)
		os.execute(command)
	else
		print('[Platform] ', platform)
		print('There is an invalid platform defined in the local platform variable at the top of the code.')
	end
	
	print('[Opening Directory] ' .. dir)
end

function win.On.Reveal.Clicked(ev)
    local tool = comp.ActiveTool or bmd.GetSavers(comp)[1]
    if not tool or not tool.ID == "Saver" then
        return
    end
	sourceMediaFile = tool.Clip[1]
	-- Open up the folder where the media is located
	mediaFolder = Dirname(sourceMediaFile)
	if bmd.fileexists(mediaFolder) then
		OpenDirectory(mediaFolder)
	end
end

-- The window was closed
function win.On.SManager.Close(ev)
    disp:ExitLoop()
end

function check_selected(tool)
    return tool:GetAttrs('TOOLB_Selected')
end

function check_enabled(tool)
    return tool:GetAttrs('TOOLB_PassThrough')
end
    
function win.On.setIn.Clicked (ev)
    local tool = comp:GetToolList(true, 'Saver')[1]
    if tool then
        tool:SetAttrs({TOOLNT_EnabledRegion_Start = comp.CurrentTime})
    end
end

function win.On.setOut.Clicked (ev)
    local tool = comp:GetToolList(true, 'Saver')[1]
    if tool then
        tool:SetAttrs({TOOLNT_EnabledRegion_End = comp.CurrentTime})
    end
end

function win.On.Solo.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local selectedSavers = cmp:GetToolList(true, "Saver")
    local allSavers = cmp:GetToolList(false, "Saver")
    for _, currentSaver in pairs(allSavers) do
        if not check_selected(currentSaver) then
            currentSaver:SetAttrs( { TOOLB_PassThrough = true } )
        end
    end
    for _, sel in pairs(selectedSavers) do
        if check_enabled(sel) then
            sel:SetAttrs({ TOOLB_PassThrough = false})
        end
    end
end

function win.On.Enable.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local allSavers = cmp:GetToolList(false, "Saver")
        for i, currentSaver in pairs(allSavers) do
            currentSaver:SetAttrs( { TOOLB_PassThrough = false } )
        end
end

function win.On.SelectAll.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    local allSavers = cmp:GetToolList(false, "Saver")
    flow = cmp.CurrentFrame.FlowView
    for _, sav in pairs(allSavers) do
        flow:Select(sav)
    end
end

function win.On.CreateLoaders.Clicked(ev)
    local cmp = fu:GetCurrentComp()
    cmp:RunScript('Scripts:Comp/Saver Tools/LoaderFromSaver.lua')
end

win:Show()
disp:RunLoop()
win:Hide()
